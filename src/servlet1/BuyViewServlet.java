package servlet1;

import java.io.IOException;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import servlet1.dao.BuyDAO;
import servlet1.webshop.Buy;
import servlet1.webshop.User;

/**
 * Servlet implementation class BuyViewServlet
 */
public class BuyViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyViewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user")==null){
			response.sendRedirect("login.html");
			return;
		}
		User u = (User) session.getAttribute("user");
		BuyDAO buyDao = new BuyDAO();
		List<Buy> buyList = buyDao.getAll(u.getId());
		
		session.setAttribute("buyList", buyList);
		
		response.sendRedirect("BuyPage.jsp");
	}

}
