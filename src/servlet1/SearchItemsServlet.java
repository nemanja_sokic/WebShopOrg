package servlet1;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import servlet1.dao.ProductsDAO;
import servlet1.webshop.Product;

/**
 * Servlet implementation class SearchItemsServlet
 */
public class SearchItemsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchItemsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		HttpSession session = request.getSession();
		if(session.getAttribute("user")==null){
			response.sendRedirect("login.html");
			return;
		}
		
		String text = request.getParameter("searchValue");
		if(text == null){
			text = "";
		}
		ProductsDAO pDao = new ProductsDAO();
		List<Product> products = pDao.getAll();
		List<Product> foundProducts = pDao.searchProducts(text,products);
		
		session.setAttribute("foundProducts", foundProducts);
		response.sendRedirect("SearchItemsServlet.jsp");
		
	}

}
