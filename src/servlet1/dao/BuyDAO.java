package servlet1.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import servlet1.webshop.Buy;
import servlet1.webshop.Product;
import servlet1.webshop.ShoppingCartItem;
import servlet1.webshop.User;
import util.ConnectionManager;

public class BuyDAO {

	public void buyProducts(List<ShoppingCartItem> items,User u) {
		String s = "insert into buy (count,User_id,Product_id) values (?,?,?);";
		
		
		try {
			PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(s);
			for (ShoppingCartItem item : items) {
				ps.setInt(1, item.getCount());
				ps.setInt(2, u.getId());
				ps.setInt(3, item.getProduct().getId());
				
				
				ps.executeUpdate();
			}
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public List<Buy> getAll(int UserId){
		List<Buy> lista = new ArrayList<Buy>();
		
		String s = "select buy.buy_id, p.id, p.name, p.price, buy.count, buy.date from buy join product p on buy.Product_id=p.id join user on buy.User_id=user.id where user.id = "+ UserId +";";
		try {
			Statement st = ConnectionManager.getConnection().createStatement();
			ResultSet rs = st.executeQuery(s);
			
			UserDAO userDao = new UserDAO();
			
			
			while(rs.next()){
				int id = rs.getInt(1);
				int prodId = rs.getInt(2);
				String name = rs.getString(3);
				Double price = rs.getDouble(4);
				Product p = new Product(prodId,name,price);
				int count = rs.getInt(5);
				Date d = rs.getDate(6);
				User u = userDao.getUserByID(UserId);
				Buy b = new Buy(id,u,p,count,d);
				lista.add(b);
			}
			
			st.close();
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;
	}

}
