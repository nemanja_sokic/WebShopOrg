package servlet1.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import servlet1.webshop.Product;
import servlet1.webshop.ShoppingCartItem;
import util.ConnectionManager;

public class ShoppingCartDAO {

	public void getItemToShoppingCart(int count,int userId, int productId){
		
		String s = "insert into shoppingcartitem (count,User_id,Product_id) values(?,?,?);";
		
		try {
			PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(s);
			
			ps.setInt(1, count);
			ps.setInt(2, userId);
			ps.setInt(3, productId);
			
			if(ps.executeUpdate()==1){
				System.out.println("Uspesno unet slog");
			}
			ConnectionManager.closeConnection();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public List<ShoppingCartItem> getItemByUser(int userId){
		List<ShoppingCartItem> items = new ArrayList<ShoppingCartItem>();
		//select p.name,p.price,s.count from shoppingcartitem s join product p on s.Product_id = p.id where s.User_id = 1;
		String s = "select p.id, s.id, p.name,p.price,s.count from shoppingcartitem s join product p on s.Product_id = p.id where s.User_id = "+ userId +";";
		ResultSet rs = null;
		Statement st;
		try {
			st = ConnectionManager.getConnection().createStatement();
			rs = st.executeQuery(s);
			ShoppingCartItem sc;


		
			while(rs.next()){
				
				int idProd = rs.getInt(1);
				String name = rs.getString(3);
				double price = rs.getDouble(4);
				int id = rs.getInt(2);
				int count = rs.getInt(5);
				sc = new ShoppingCartItem(id,new Product(idProd,name,price),count);
				items.add(sc);
			}			
			st.close();
			rs.close();
			
			ConnectionManager.closeConnection();
		} catch (SQLException e) {
			System.out.println("Greska kod ShoppingCartDAO");
			e.printStackTrace();
		}
		
		return items;
	}
	public void deleteProductFromCart(int id){
		String s = "delete from shoppingcartitem where id = ? ;";
		try {
			PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(s);
			ps.setInt(1, id);
			ps.executeUpdate();
			ps.close();
			ConnectionManager.closeConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
