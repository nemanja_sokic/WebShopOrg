package servlet1.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import servlet1.webshop.Product;
import util.ConnectionManager;

public class ProductsDAO {
	
	public List<Product> getAll(){
		List<Product> lista = new ArrayList<Product>();
		String s = "select id, name, price from product;";
		
		try {
			Statement st = ConnectionManager.getConnection().createStatement();
			ResultSet rs = st.executeQuery(s);
			while(rs.next()){
				int id = rs.getInt(1);
				String name = rs.getString(2);
				Double price = rs.getDouble(3);
				
				Product p = new Product(id,name,price);
				lista.add(p);
			}
			st.close();
			rs.close();
			ConnectionManager.closeConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lista;
	}

	public Product getProductById(int idProd) {
		String s = "select id, name, price from product where id = "+ idProd +";";
		Product p = null;
		try {
			Statement st = ConnectionManager.getConnection().createStatement();
			ResultSet rs1 = st.executeQuery(s);
			if(rs1.next()){
				int id = rs1.getInt(1);
				String name = rs1.getString(2);
				Double price = rs1.getDouble(3);
				
				p = new Product(id,name,price);
				
			}
			st.close();
			rs1.close();
			ConnectionManager.closeConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return p;
	}

	public List<Product> searchProducts(String text, List<Product> products) {
		List<Product> found = new ArrayList<Product>();
		
		for (int i = 0; i < products.size(); i++) {
			if(products.get(i).getName().startsWith(text)){
				found.add(products.get(i));
			}
		}
		
		return found;
	}
	
	
		
		
	
	
}
