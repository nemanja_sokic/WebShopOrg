package servlet1;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import servlet1.dao.ShoppingCartDAO;
import servlet1.webshop.ShoppingCartItem;
import servlet1.webshop.User;

/**
 * Klasa koja obavlja listanje stavki u korpi, a ako je pozvana iz forme, dodaje
 * jednu stavku u korpu, pa onda lista).
 */
public class ShoppingCartServlet extends HttpServlet {

	private static final long serialVersionUID = -8628509500586512294L;

	

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		HttpSession session = request.getSession();
		if(session.getAttribute("user")==null){
			response.sendRedirect("login.html");
			return;
		}
		
		ShoppingCartDAO scDao = new ShoppingCartDAO();
		
		
		response.setContentType("text/html");
		 
		String count1 = request.getParameter("itemCount");
		if(count1 != null){
			int count = Integer.parseInt(count1);
			User u = (User) session.getAttribute("user");
			int idUser = u.getId();
			int idP = Integer.parseInt(request.getParameter("itemId"));
			scDao.getItemToShoppingCart(count, idUser, idP);
			
		}
			User u = (User) session.getAttribute("user");
			int idUser = u.getId();
			List <ShoppingCartItem>	sc = scDao.getItemByUser(idUser);
		
			session.setAttribute("sc", sc);
			response.sendRedirect("ShoppingCart.jsp");
	}
		
		
}
