package servlet1.webshop;

import java.sql.Date;

public class Buy {
	
	int buy_id;
	User u;
	Product p;
	int count;
	Date date;
	public Buy(int buy_id, User u, Product p, int count, Date date) {
		super();
		this.buy_id = buy_id;
		this.u = u;
		this.p = p;
		this.count = count;
		this.date = date;
	}
	public int getBuy_id() {
		return buy_id;
	}
	public void setBuy_id(int buy_id) {
		this.buy_id = buy_id;
	}
	public User getU() {
		return u;
	}
	public void setU(User u) {
		this.u = u;
	}
	public Product getP() {
		return p;
	}
	public void setP(Product p) {
		this.p = p;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
	
}
