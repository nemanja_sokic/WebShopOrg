package servlet1;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import servlet1.dao.ProductsDAO;
import servlet1.webshop.Product;

/**
 * Osnovni servlet koji lista raspolozive proizvode i omogucuje njihovo
 * dodavanje u korpu.
 */
public class WebShopServlet extends HttpServlet {

	private static final long serialVersionUID = 6593194247788949676L;
	

	/**
	 * Atribut se dodaje u application scope, da bi se video iz klase ShoppingCartServlet.
	 */
	private ProductsDAO pDao = new ProductsDAO();
	private List<Product> products = pDao.getAll();
	/*
	 * Obratiti paznju da se metod init() zove samo jednom, prilikom prvo pokretanja (inicijalziacije)
	 * servleta.
	 * => Ukoliko bismo u products.txt dodali novi proizvod, bez restartovanja web servera, a prethodno
	 * je servlet vec bio pokrenut, novi proizvod se nece biti procitan.
	 */
	@Override
	public void init(ServletConfig cfg) {
		try {
			// obavezan poziv super metode, kako bi se korektno izvrsila inicijalizacija
			super.init(cfg);
		} catch (ServletException e) {
			e.printStackTrace();
		}
		
		
		ServletContext ctx = getServletContext();
	//	products = new Products(ctx.getRealPath(""));
		ctx.setAttribute("products", products);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		HttpSession session = request.getSession();
		if(session.getAttribute("user")==null){
			response.sendRedirect("login.html");
			return;
		}

		
		session.setAttribute("products", products);
		
		response.sendRedirect("WebShop.jsp");
		
		
		
		
	}
}