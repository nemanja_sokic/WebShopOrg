package servlet1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import servlet1.dao.BuyDAO;
import servlet1.dao.ShoppingCartDAO;
import servlet1.webshop.ShoppingCartItem;
import servlet1.webshop.User;

/**
 * Servlet implementation class BuyServlet
 */
public class BuyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public BuyServlet() {
        super();
        
    }

	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		
	}

	
	@SuppressWarnings({ "unchecked"})
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		HttpSession session = request.getSession();
		if(session.getAttribute("user")==null){
			response.sendRedirect("login.html");
			return;
		}
		
		List<ShoppingCartItem> items = (ArrayList<ShoppingCartItem>) request.getSession().getAttribute("sc");
		BuyDAO buyDao = new BuyDAO();
		
		buyDao.buyProducts(items,(User)session.getAttribute("user"));
		
		ShoppingCartDAO scDao = new ShoppingCartDAO();
		
		for (ShoppingCartItem item : items) {
			scDao.deleteProductFromCart(item.getId());
		}
		response.sendRedirect("ShoppingCartServlet");
	}

}
