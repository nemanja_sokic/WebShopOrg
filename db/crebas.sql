SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema jwd13_11
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `jwd13_11` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `jwd13_11` ;

-- -----------------------------------------------------
-- Table `jwd13_11`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jwd13_11`.`User` ;

CREATE TABLE IF NOT EXISTS `jwd13_11`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jwd13_11`.`Product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jwd13_11`.`Product` ;

CREATE TABLE IF NOT EXISTS `jwd13_11`.`Product` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `price` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jwd13_11`.`ShoppingCartItem`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jwd13_11`.`ShoppingCartItem` ;

CREATE TABLE IF NOT EXISTS `jwd13_11`.`ShoppingCartItem` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `count` INT NULL,
  `User_id` INT NOT NULL,
  `Product_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ShoppingCartItem_User_idx` (`User_id` ASC),
  INDEX `fk_ShoppingCartItem_Product1_idx` (`Product_id` ASC),
  CONSTRAINT `fk_ShoppingCartItem_User`
    FOREIGN KEY (`User_id`)
    REFERENCES `jwd13_11`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ShoppingCartItem_Product1`
    FOREIGN KEY (`Product_id`)
    REFERENCES `jwd13_11`.`Product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `jwd13_11`.`Buy` (
  `buy_id` INT NOT NULL AUTO_INCREMENT,
  `count` INT NULL,
  `User_id` INT NOT NULL,
  `Product_id` INT NOT NULL,
  `date` datetime NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`buy_id`),
   FOREIGN KEY (`User_id`)
    REFERENCES `jwd13_11`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    FOREIGN KEY (`Product_id`)
    REFERENCES `jwd13_11`.`Product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
  );

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `jwd13_11`.`User`
-- -----------------------------------------------------
START TRANSACTION;
USE `jwd13_11`;
INSERT INTO `jwd13_11`.`User` (`id`, `username`, `password`) VALUES (NULL, 'pera', 'peric');
INSERT INTO `jwd13_11`.`User` (`id`, `username`, `password`) VALUES (NULL, 'steva', 'stevic');
Insert into `jwd13_11`.`Product` (name,price) values ('Grasak',250);
Insert into `jwd13_11`.`Product` (name,price) values ('Kelj',80);
Insert into `jwd13_11`.`Product` (name,price) values ('Paradajz',120);
Insert into `jwd13_11`.`Product` (name,price) values ('Boranija',150);
Insert into `jwd13_11`.`Product` (name,price) values ('Pasulj',300);

COMMIT;

