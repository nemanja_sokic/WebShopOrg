<%@page import="servlet1.webshop.Buy"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Buy</title>
</head>
<body>
Svi kupljeni proizvodi korisnika:

<% ArrayList<Buy> listaBuy =(ArrayList<Buy>) request.getSession().getAttribute("buyList");
double t = 0;
%>



<table border="2" background="grey">
<tr bgcolor="lightgrey">
<th>Product</th><th>Price</th><th>count</th><th>Date</th><th>total</th>
</tr>
<%for(Buy buy : listaBuy){ 
	double total = buy.getP().getPrice()*buy.getCount();
%>
<tr>
<td><%=buy.getP().getName() %></td>
<td><%=buy.getP().getPrice() %></td>
<td><%=buy.getCount() %></td>
<td><%=buy.getDate() %></td>
<td><%=total %></td>
</tr>
<% t +=total; }%>


</table>
<p>Ukupno potroseno <%=t %> dinara.</p>
<a href="WebShopServlet">Nazad</a>
</body>
</html>