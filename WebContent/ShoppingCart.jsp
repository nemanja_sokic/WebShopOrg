<%@page import="java.util.List"%>
<%@page import="servlet1.webshop.ShoppingCartItem"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Shopping Cart</title>
</head>
<body>
<% List <ShoppingCartItem>	sc = (List<ShoppingCartItem>) request.getSession().getAttribute("sc"); %>
Proizvodi u korpi:
<table><tr bgcolor="lightgrey"><th>Naziv</th><th>Jedinicna cena</th><th>Komada</th><th>Ukupna cena</th><th>&nbsp;</th></tr>
<%double total = 0;
for ( ShoppingCartItem i : sc) { %>
<tr>
<td><%=i.getProduct().getName() %></td>
<td><%=i.getProduct().getPrice() %></td>
<td><%=i.getCount() %></td>
<% double price = i.getProduct().getPrice() * i.getCount(); %>
<td><%= price %></td>
<td><form action="DeleteServlet" method="post"><input type="hidden" name="id" value="<%=i.getId()  %>"><input type="submit" value="delete"></form></td>
</tr>
<% total += price; %>
<% } %>
</table>
<p>
Ukupno <%= total %> dinara.
</p>
<a href="BuyServlet">Kupi</a>
<a href="BuyViewServlet">Pregled kupovina</a><br/>
<a href="WebShopServlet">Nazad</a>
</body>
</html>



		<!-- pout.println("<html>");
		pout.println("<head>");
		pout.println("</head>");
		pout.println("<body>");
		pout.println("Proizvodi u korpi:");
		pout.println("<table><tr bgcolor=\"lightgrey\"><th>Naziv</th><th>Jedinicna cena</th><th>Komada</th><th>Ukupna cena</th><th>&nbsp;</th></tr>");
		double total = 0;
		for ( ShoppingCartItem i : sc) {
			pout.println("<tr>");
			pout.println("<td>" + i.getProduct().getName() + "</td>");
			pout.println("<td>" + i.getProduct().getPrice() + "</td>");
			pout.println("<td>" + i.getCount() + "</td>");
			double price = i.getProduct().getPrice() * i.getCount();
			pout.println("<td>" + price + "</td>");
			pout.println("<td><form action=\"DeleteServlet\" method=\"post\">"
					+ "<input type=\"hidden\" name=\"id\" value=\"" + i.getId() + "\">"
							+ "<input type=\"submit\" value=\"delete\"></form></td>");
			pout.println("</tr>");
			total += price;
		}
		pout.println("</table>");

		pout.println("<p>");
		pout.println("Ukupno: " + total + " dinara.");
		pout.println("</p>");

		pout.println("<p>");
		pout.println("<a href=\"WebShopServlet\">Povratak</a>");
		pout.println("</p>");

		pout.println("</body>");
		pout.println("</html>");
	} -->