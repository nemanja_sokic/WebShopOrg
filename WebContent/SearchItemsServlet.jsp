<%@page import="java.util.List"%>
<%@page import="servlet1.webshop.Product"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search Item</title>
</head>
<body>
<% List<Product> foundProducts = (List<Product>) request.getSession().getAttribute("foundProducts"); %>
Pronadjeni proizvodi:
<form action="SearchItemsServlet" method="get"><input type="text" name="searchValue"/></form>
<table border="1"><tr bgcolor="lightgrey"><th>Naziv</th><th>Cena</th></tr>
<% for ( Product p : foundProducts ) { %>
<tr>
<form method="get" action="ShoppingCartServlet">
<td><%= p.getName()%> </td>
<td><%= p.getPrice()%> </td>
</tr>
</form>
<% } %>
</table>
<p>
<a href="ShoppingCartServlet">Pregled sadrzaja korpe</a><br/>
<a href="WebShopServlet">Nazad</a>
</p>
</body>
</html>




		
<!-- 		pout.println("<html>");
		pout.println("<head>");
		pout.println("</head>");
		pout.println("<body>");
		pout.println("Raspolozivi proizvodi:");
		pout.println("<form action=\"SearchItemsServlet\" method=\"get\"><input type=\"text\" name=\"searchValue\"/></form>");
		pout.println("<table border=\"1\"><tr bgcolor=\"lightgrey\"><th>Naziv</th><th>Cena</th></tr>");
		for ( Product p : foundProducts ) {
			pout.println("<tr>");
			pout.println("<form method=\"get\" action=\"ShoppingCartServlet\">");
			pout.println("<td>" + p.getName() + "</td>");
			pout.println("<td>" + p.getPrice() + "</td>");
			pout.println("</tr>");
		}
		pout.println("</table>");

		pout.println("<p>");
		pout.println("<a href=\"ShoppingCartServlet\">Pregled sadrzaja korpe</a><br/>");
		pout.println("<a href=\"WebShopServlet\">Nazad</a>");
		pout.println("</p>");

		pout.println("</body>");
		pout.println("</html>"); -->