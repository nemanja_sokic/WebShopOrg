<%@page import="java.util.List"%>
<%@page import="servlet1.webshop.Product"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Web Shop</title>
</head>
<body>
Raspolozivi proizvodi:
<table border="1"><tr bgcolor="lightgrey"><th>Naziv</th><th>Cena</th><th>&nbsp;</th></tr>
<% List<Product> products = (List<Product>) request.getSession().getAttribute("products"); %>
<% for ( Product p : products) { %>
<tr>
<form method="get" action="ShoppingCartServlet">
<td><%=p.getName() %></td>
<td><%=p.getPrice() %></td>
<td>
<input type="text" size="3" name="itemCount">
<input type="hidden" name="itemId" value="<%=p.getId() %>">
<input type="submit" value="Dodaj">
</form>
</td>
</tr>
<% } %>
</table><br/>

Pretraga proizvoda: 
<form action="SearchItemsServlet">
<input type="text" name="searchValue"><input type="submit" value="search"></form>
<p>
<a href="ShoppingCartServlet">Pregled sadrzaja korpe</a><br/>
<a href="BuyViewServlet">Pregled kupovina</a><br/>
<a href="index.html">Pocetna</a>
</p>
</body>
</html>



<!-- pout.println("<html>");
		pout.println("<head>");
		pout.println("</head>");
		pout.println("<body>");
		pout.println("Raspolozivi proizvodi:");
		
		pout.println("<table border=\"1\"><tr bgcolor=\"lightgrey\"><th>Naziv</th><th>Cena</th><th>&nbsp;</th></tr>");
		for ( Product p : products) {
			pout.println("<tr>");
			pout.println("<form method=\"get\" action=\"ShoppingCartServlet\">");
			pout.println("<td>" + p.getName() + "</td>");
			pout.println("<td>" + p.getPrice() + "</td>");
			pout.println("<td>");
			pout.println("<input type=\"text\" size=\"3\" name=\"itemCount\">");
			pout.println("<input type=\"hidden\" name=\"itemId\" value=\"" + p.getId() + "\">");
			pout.println("<input type=\"submit\" value=\"Dodaj\">");
			pout.println("</form>");
			pout.println("</td>");
			pout.println("</tr>");
		}
		pout.println("</table><br/>");
		pout.println("Pretraga proizvoda: ");
		pout.println("<form action=\"SearchItemsServlet\">");
		pout.println("<input type=\"text\" name=\"searchValue\"><input type=\"submit\" value=\"search\"></form>");
		pout.println("<p>");
		pout.println("<a href=\"ShoppingCartServlet\">Pregled sadrzaja korpe</a><br/>");
		pout.println("<a href=\"index.html\">Pocetna</a>");
		pout.println("</p>");

		pout.println("</body>");
		pout.println("</html>"); -->